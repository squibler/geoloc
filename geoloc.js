jQuery(document).ready( function() {
    if( navigator.geolocation ) {
        var geoloc_success = function( position ) {
            jQuery.post( '/wp-json/geoloc/v1/add', {
                location: {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                }
            });
        };
        navigator.geolocation.getCurrentPosition( geoloc_success );
    }
});