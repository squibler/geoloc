<?php
/**
 * Plugin Name:     Geoloc
 * Description:     Ask for and retrive visitor locations
 * Author:          Daniel E. Smith
 * Author URI:      https://www.growthmarketingagency.top
 * Version:         1.0.1
 *
 * @package         Geoloc
 */


if ( ! function_exists( 'geo_script' ) ) {
	/**
	* Add the javascript to ask for and collect the users position
	*/
	function geo_script() {
		wp_enqueue_script( 'geoloc', plugin_dir_url( __FILE__ ) . 'geoloc.js', array( 'jquery' ), true );
	}
	add_action( 'wp_enqueue_scripts', 'geo_script' );
}

if ( ! function_exists( 'geo_store_location' ) ) {
	function geo_store_location( $request ) {
		$theApiKey = 'AIzaSyBSMktFMYf14uEBcbhstns8MIBe5zfNvgQ';
		$theCoords = $request->get_param( 'location' );

		// Create the google rest request
		$theGoogleRequest = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='
						. $theCoords['latitude'] . ','
						. $theCoords['longitude']
						. '&key=' . $theApiKey;

		$result = wp_remote_get( $theGoogleRequest );

		// If there is a wordpress error
		if( $result instanceof WP_Error ) {
			return json_encode( array( "error" => true, 'message' => $result->get_Error_message() ) );
		}

		// Parse the result from google
		$location = json_decode( $result['body'] );

		// If there was any error getting the info from google
		if( $location->status !== 'OK' ) {
			return json_encode( array( "error" => true, 'message' => $location->status ) );
		}

		$address = $location->results[0]->formatted_address;
		$floc = __DIR__ . DIRECTORY_SEPARATOR . 'geo_locations.txt';
		$file = fopen( $floc, 'ab+' );
		fwrite( $file, '[' . date( 'r' ) . '] ' . $address . PHP_EOL );
		fclose( $file );

		return  json_encode( array( 'success' => true, 'location' => $address ) );
	}

	add_action( 'rest_api_init', function () {
		register_rest_route( 'geoloc/v1', '/add', array(
			'methods' => 'POST',
			'callback' => 'geo_store_location',
		));
	});
}